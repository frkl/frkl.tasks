#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_tasks` package."""

import pytest  # noqa
from frkl.tasks.task import SingleTaskAsync, Task


@pytest.mark.anyio
async def test_single_task():
    def wrapped_func():
        return {"x": "y"}

    task: Task = SingleTaskAsync(func=wrapped_func)

    result = await task.run_async()
    assert result.success is True
    assert result.result_value == {"x": "y"}


@pytest.mark.anyio
async def test_single_task_fail():
    def wrapped_func():
        return x + 1  # noqa

    task: Task = SingleTaskAsync(func=wrapped_func)

    result = await task.run_async()
    assert result.success is False
    assert result.result_value is None
    assert isinstance(result.error, NameError)


@pytest.mark.anyio
async def test_single_task_side_effect():

    task: Task = SingleTaskAsync(func=print, func_args=["hello world"])

    result = await task.run_async()
    assert result.result_value is None
    assert result.success is True
