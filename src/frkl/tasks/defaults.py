# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


frkl_tasks_app_dirs = AppDirs("frkl_tasks", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_TASKS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_tasks` module."""
else:
    FRKL_TASKS_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_tasks"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_tasks` module."""

FRKL_TASKS_RESOURCES_FOLDER = os.path.join(FRKL_TASKS_MODULE_BASE_FOLDER, "resources")
