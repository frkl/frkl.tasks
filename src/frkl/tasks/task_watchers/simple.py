# -*- coding: utf-8 -*-
from typing import Any, Iterable, Mapping, Optional

from frkl.tasks.task_desc import TaskDesc
from frkl.tasks.task_watchers import TaskWatcher


class SimpleTaskWatcher(TaskWatcher):

    _plugin_name = "simple"

    def __init__(
        self,
        base_topics: Optional[Iterable[str]] = None,
        root_task: TaskDesc = None,
        **kwargs,
    ):

        super().__init__(base_topics=base_topics, root_task=root_task)

    def _task_started(self, source: TaskDesc, event_details: Mapping[str, Any]):
        print(f"task started: {source.name}")

    def _task_finished(self, source: TaskDesc, event_details: Mapping[str, Any]):

        print(f"task finished: {source.name}")

    def _task_failed(self, source: TaskDesc, event_details: Mapping[str, Any]):

        print(f"task failed: {source.name} - {source.get_failed_msg()}")
