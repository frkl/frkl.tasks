# -*- coding: utf-8 -*-
from typing import Any, Iterable, Mapping, Optional, Union

from frkl.tasks.task_desc import TaskDesc
from frkl.tasks.task_watchers import TaskWatcher
from rich.console import Console
from rich.style import Style
from rich.theme import Theme


TASK_THEME = Theme(
    {
        "started": Style.parse("dim cyan"),
        "finished": Style.parse("magenta"),
        "error": Style.parse("bold red"),
    }
)


class TerminalTaskWatcher(TaskWatcher):

    _plugin_name = "terminal"

    def __init__(
        self,
        base_topics: Optional[Iterable[str]] = None,
        root_task: Optional[TaskDesc] = None,
    ):
        self._console: Console = Console(theme=TASK_THEME)
        super().__init__(base_topics=base_topics, root_task=root_task)

    @property
    def console(self) -> Console:

        return self._console

    def print(
        self,
        *objects: Any,
        sep=" ",
        end="\n",
        style: Union[str, Style] = None,
        emoji: bool = None,
        markup: bool = None,
        highlight: bool = None,
    ):

        self._console.print(
            *objects,
            sep=sep,
            end=end,
            style=style,
            emoji=emoji,
            markup=markup,
            highlight=highlight,
        )

    def _task_started(self, source: TaskDesc, event_details: Mapping[str, Any]):

        self.print(f"task started: {source.name}", style="started")

    def _task_finished(self, source: TaskDesc, event_details: Mapping[str, Any]):

        print(f"task finished: {source.name}")

    def _task_failed(self, source: TaskDesc, event_details: Mapping[str, Any]):

        print(f"task failed: {source.name} - {source.get_failed_msg()}")
