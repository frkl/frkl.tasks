# -*- coding: utf-8 -*-
import logging
from enum import Enum
from typing import Any, Dict, Iterable, Mapping, Optional, Union

from frkl.common.defaults import FRKL_COLOR_PROGRESSION
from frkl.common.exceptions import FrklException
from frkl.common.types import isinstance_or_subclass
from frkl.tasks.task_desc import TaskDesc
from frkl.tasks.task_watchers import TaskWatcher
from rich.console import Console
from rich.progress import Progress, TaskID
from treelib import Tree


log = logging.getLogger("frkl")


class TaskStatus(Enum):

    STARTED = 1
    FINISHED = 2
    FAILED = 3


class TasksProgress(Progress):
    def __init__(
        self,
        finished_max_priority: int = -1,
        console: Optional[Console] = None,
        **kwargs,
    ):

        self._finished_max_priority: int = finished_max_priority
        self._all: Dict[str, TaskStatus] = {}

        self._task_tree: Tree = Tree()

        self._task_tree.create_node(
            tag="root", identifier="__root_node__", parent=None, data=None
        )

        kwargs["auto_refresh"] = False
        kwargs["transient"] = True

        super().__init__(console=console, **kwargs)

    def move_task_desc(self, task_desc: TaskDesc, to_task_status: TaskStatus):

        if task_desc.id not in self._all.keys():
            raise Exception(f"Task with id '{task_desc.id}' not started yet.")

        if self._all[task_desc.id] != TaskStatus.STARTED:
            raise Exception(
                f"Can't move task '{task_desc.name}' to '{to_task_status}': task not registered as started yet"
            )

        self._all[task_desc.id] = to_task_status

    def task_started(self, desc: TaskDesc, parent_id: Optional[str]):

        if desc.id in self._all.keys():
            raise Exception(f"Task alraedy added: {desc.id}")

        self._all[desc.id] = TaskStatus.STARTED

        if parent_id is None:
            parent_id = "__root_node__"

        try:
            self._task_tree.create_node(
                tag=f"{desc.name} (running)",
                identifier=desc.id,
                parent=parent_id,
                data=desc,
            )
            self.refresh()
        except Exception as e:

            log.error(f"Can't display task progress for task '{desc}': {e}")
            log.debug(
                f"Error displaying task progress for task '{desc}'", exc_info=True
            )

    def task_finished(self, desc: TaskDesc):

        self.move_task_desc(desc, TaskStatus.FINISHED)
        self.refresh()

    def task_failed(self, desc: TaskDesc):

        self.move_task_desc(desc, TaskStatus.FAILED)
        self.refresh()

    def get_renderables(self):

        yield "[title]Progress[/title]"
        yield ""
        if not self._task_tree:
            return

        # for task_id, status in self._all.items():
        for task_id in self._task_tree.expand_tree(sorting=False):

            if task_id == "__root_node__":
                continue

            node = self._task_tree.get_node(task_id)
            level = self._task_tree.level(task_id) - 1

            if level == 0:
                color = ""
                color_end = ""
            else:
                color_number = (level - 1) % 5
                color_name = FRKL_COLOR_PROGRESSION[color_number]
                color = f"[{color_name}]"
                color_end = f"[/{color_name}]"

            status = self._all[task_id]
            # desc: TaskDesc = node.data
            desc: TaskDesc = node.data
            msg_string = desc.msg

            if status == TaskStatus.STARTED:
                msg: Union[str, Iterable[str]] = f"- {msg_string}..."
            elif status == TaskStatus.FINISHED:
                if (
                    self._finished_max_priority > 0
                    and desc.priority > self._finished_max_priority
                ):
                    msg = None
                else:
                    msg = f"- {msg_string} -> [green]done[/green]"
                    # msg = desc.get_result_msg(f"{msg_string}: done")
            elif status == TaskStatus.FAILED:
                is_branch = node.is_leaf()
                msg = [f"- {msg_string} -> [red]failed[/red]"]
                if is_branch:
                    if isinstance_or_subclass(desc._exception, FrklException):
                        f = desc._exception.msg
                    else:
                        f = desc.get_failed_msg()
                    msg.append(f"  [red]Error:[/red] [dark_red]{f}[/dark_red]")

                # msg = desc.get_failed_msg(f"{msg_string}: failed")
            padding = level * "  "

            if msg is None:
                msg = []
            if isinstance(msg, str):
                msg = [msg]
            for m in msg:
                yield f"{padding}{color}{m}{color_end}".rstrip()

        # yield ""


class RichTaskWatcher(TaskWatcher):

    _plugin_name = "rich"

    def __init__(
        self,
        progress: Optional[TasksProgress] = None,
        base_topics: Optional[Iterable[str]] = None,
        root_task: TaskDesc = None,
        console: Optional[Console] = None,
        **kwargs,
    ):

        if root_task:
            raise NotImplementedError()

        if progress is None:
            progress = TasksProgress(console=console)

        self._progress: TasksProgress = progress
        self._task: TaskID = self._progress.add_task("starting task", total=10)

        self._progress_started: bool = False

        super().__init__(base_topics=base_topics, root_task=root_task)

    def start(self):

        super().start()

    def stop(self):

        super().stop()
        self.progress.stop()

    @property
    def progress(self) -> TasksProgress:

        return self._progress

    def _task_started(self, source: TaskDesc, event_details: Mapping[str, Any]):

        if not self._progress_started and (
            self._started_tasks - self._finished_tasks > 0
        ):
            self.progress.start()

        if source == self._root_task:
            parent_id: Optional[str] = None
        elif source.parent:
            parent_id = source.parent.id
        else:
            parent_id = None

        self.progress.task_started(source, parent_id)

    def _task_finished(self, source: TaskDesc, event_details: Mapping[str, Any]):

        self.progress.task_finished(source)

        if self._started_tasks - self._finished_tasks == 0:
            self._progress.stop()

    def _task_failed(self, source: TaskDesc, event_details: Mapping[str, Any]):

        self.progress.task_failed(source)

        if self._started_tasks - self._finished_tasks == 0:
            self._progress.stop()
