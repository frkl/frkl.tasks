# -*- coding: utf-8 -*-
# from typing import Any, Dict, Iterable, Mapping, Optional
#
# from frkl.tasks.task_desc import TaskDesc
# from frkl.tasks.task_watchers import TaskWatcher, log
# from treelib import Tree
#
#
# class TreeTaskWatcher(TaskWatcher):
#
#     _plugin_name = "tree"
#
#     def __init__(
#         self,
#         base_topics: Optional[Iterable[str]] = None,
#         sort_task_names: bool = True,
#         root_task: TaskDesc = None,
#         **kwargs,
#     ):
#
#         self._old_height = 0
#         self._sort_task_names = sort_task_names
#
#         self._tasks: Tree = Tree()
#
#         # self._root_tasks: Node = self._tasks.create_node(
#         #     tag=f"{root_task.name}", identifier="root", data=root_task
#         # )
#
#         self._running_tasks: Dict[str, TaskDesc] = {}
#         self._finished_tasks: Dict[str, TaskDesc] = {}
#
#         super().__init__(base_topics=base_topics, root_task=root_task)
#
#         self._debug = False
#
#     def _task_started(self, source: TaskDesc, event_details: Mapping[str, Any]):
#
#         if source == self._root_task:
#             parent_id: Optional[str] = None
#         elif source.parent:
#             parent_id = source.parent.id
#         else:
#             if self._root_task is None:
#                 self._root_task = TaskDesc(name="tasks")
#                 self._tasks.create_node(
#                     tag=f"{self._root_task.name}",
#                     identifier=self._root_task.id,
#                     data=self._root_task,
#                 )
#
#             parent_id = self._root_task.id
#
#         if self._tasks.get_node(source.id):
#             raise Exception(
#                 f"Task with id '{source.id}' already registered: {self._tasks.get_node(source.id).data} -- {source}"
#             )
#
#         if source.id in self._running_tasks.keys():
#             raise Exception(
#                 f"Task with id '{source.id}' already registered: {self._running_tasks[source.id]} -- {source}"
#             )
#
#         try:
#             self._tasks.create_node(
#                 tag=f"{source.name} (running)",
#                 identifier=source.id,
#                 parent=parent_id,
#                 data=source,
#             )
#             self._running_tasks[source.id] = source
#
#             self.update_text()
#         except Exception as e:
#             self._tasks.show()
#             self._old_height = 0
#
#             log.error(f"Can't display task progress for task '{source}': {e}")
#             log.debug(
#                 f"Error displaying task progress for task '{source}'", exc_info=True
#             )
#
#     def _task_finished(self, source: TaskDesc, event_details: Mapping[str, Any]):
#
#         if source.id not in self._running_tasks.keys():
#             # self._task_started(source, event_details={})
#             raise Exception(f"Task with id '{source.id}' not registered as started.")
#
#         t = self._tasks.get_node(source.id)
#         t.tag = f"{source.name} (finished)"
#
#         self._running_tasks.pop(source.id)
#         self._finished_tasks[source.id] = source
#
#         self.update_text()
#
#     def _task_failed(self, source: TaskDesc, event_details: Mapping[str, Any]):
#
#         if source.id not in self._running_tasks.keys():
#             # self._task_started(source, event_details={})
#             raise Exception(f"Task with id '{source.id}' not registered as started.")
#
#         t = self._tasks.get_node(source.id)
#         failed_msg = source.get_failed_msg()
#         tag = f"{source.name} (failed: {failed_msg})"
#         if len(tag) >= self._terminal.width:
#             tag = tag[0:64] + ".."
#         t.tag = tag
#
#         self._running_tasks.pop(source.id)
#         self._finished_tasks[source.id] = source
#
#         self.update_text()
#
#     def update_text(self):
#
#         t = self._terminal
#
#         # go to top of generated output
#         move_up = []
#
#         if not self._debug:
#
#             # print("OLD: {}".format(self._old_height))
#             for _ in range(self._old_height):
#                 move_up.append(t.move_up)
#                 move_up.append(t.clear_eol)
#             self._terminal.stream.write("".join(move_up))
#         self._tasks.show(key=lambda node: node.data._start_time)
#         self._old_height = self._tasks.size() + 1
#         # print("NEW: {}".format(self._old_height))
