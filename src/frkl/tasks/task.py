# -*- coding: utf-8 -*-
import inspect
import logging
from abc import ABCMeta, abstractmethod
from datetime import datetime
from functools import total_ordering
from typing import TYPE_CHECKING, Any, Callable, Dict, Iterable, Mapping, Optional, Type

from frkl.common.exceptions import FrklException
from frkl.common.strings import from_camel_case, generate_valid_identifier
from frkl.common.types import isinstance_or_subclass
from frkl.tasks.exceptions import FrklTaskException, FrklTaskRunException
from frkl.tasks.task_desc import TaskDesc
from tzlocal import get_localzone


if TYPE_CHECKING:
    from frkl.tasks.executor import TaskExecutor

log = logging.getLogger("frkl")


class TaskResult(object):
    def __init__(self, task_desc: TaskDesc):

        self._task_desc: TaskDesc = task_desc

        self._is_finished: bool = False

        self._result_value: Any = None
        self._error: Optional[Exception] = None

        self._success: Optional[bool] = None
        self._start_time: Optional[datetime] = None
        self._end_time: Optional[datetime] = None

    @property
    def task_desc(self) -> TaskDesc:
        return self._task_desc

    @task_desc.setter
    def task_desc(self, task_desc) -> None:

        if self._is_finished:
            raise FrklException(
                msg="Can't set task description.", reason="Task already finished."
            )

        self._task_desc = task_desc

    @property
    def is_finished(self):
        return self._is_finished

    def _check_finished(self):

        if not self._is_finished:
            raise Exception("Task not finished yet.")

    @property
    def result_value(self) -> Any:
        self._check_finished()
        return self._result_value

    @property
    def error(self) -> Optional[Exception]:
        self._check_finished()
        return self._error

    @property
    def success(self) -> bool:
        self._check_finished()
        return self._success  # type: ignore

    @property
    def start_time(self) -> datetime:
        self._check_finished()
        return self._start_time  # type: ignore

    @property
    def end_time(self) -> datetime:
        self._check_finished()
        return self._end_time  # type: ignore

    def set_task_result(
        self,
        result_value: Any,
        success: bool,
        start_time: datetime,
        end_time: datetime,
        error: Optional[Exception] = None,
    ) -> None:

        if self.is_finished:
            raise Exception("Task result already set.")

        self._is_finished = True

        self._result_value = result_value
        self._error = error
        self._success = success
        self._start_time = start_time
        self._end_time = end_time

    def to_dict(self) -> Dict[str, Any]:

        result = {}
        result["finished"] = self.is_finished
        if self.is_finished:
            result["start_time"] = self.start_time
            result["end_time"] = self.end_time
            result["success"] = self.success

            if self.success:
                result["value"] = self.result_value
            else:
                result["error"] = self.error
        return result

    def __repr__(self):
        success_string = ""
        if self._is_finished:
            success_string = f" success={self._success}"
        return f"{self.__class__.__name__}(id={self._task_desc.id} finished={self.is_finished}{success_string})"


@total_ordering  # type: ignore
class Task(metaclass=ABCMeta):
    def __init__(
        self,
        task_desc: Optional[TaskDesc] = None,
        parent_task: Optional["Task"] = None,
        result_type: Optional[Type] = None,
        executor: Optional["TaskExecutor"] = None,
        task_name: Optional[str] = None,
        **kwargs,
    ):

        if not task_name:
            task_name = from_camel_case(self.__class__.__name__)

        self._id = generate_valid_identifier(
            prefix=f"{task_name}_", length_without_prefix=8
        )

        self._started: bool = False
        self._finished: bool = False

        self._success: Optional[bool] = None

        self._start_time: Optional[datetime] = None
        self._end_time: Optional[datetime] = None

        self._executor: Optional[TaskExecutor] = executor

        tz = get_localzone()
        self._created_time: datetime = tz.localize(datetime.now(), is_dst=None)

        if not task_desc:
            task_desc = TaskDesc(
                id=self._id, created=self._created_time, has_subtasks=self.has_subtasks
            )
        self._task_desc: TaskDesc = task_desc

        self._parent_task: Optional[Task] = parent_task
        if self._parent_task:
            self._task_desc.parent = self._parent_task.task_desc

        if result_type is None:
            result_type = TaskResult
        elif not (result_type == TaskResult or issubclass(result_type, TaskResult)):
            raise TypeError(
                f"Invalid result type '{result_type}': must be subclass of TaskResult."
            )
        self._result_type: Type = result_type
        self._result: TaskResult = self._result_type(task_desc=task_desc)

    @property
    def id(self):
        return self._id

    @property
    def result_type(self) -> Type:
        return self._result_type

    @property
    def result(self) -> TaskResult:
        return self._result

    @property
    def has_subtasks(self) -> bool:
        return False

    @property
    def task_desc(self) -> TaskDesc:
        return self._task_desc

    @task_desc.setter
    def task_desc(self, task_desc: TaskDesc) -> None:

        if self.is_started:
            raise FrklTaskException(
                msg=f"Can't set task description for task '{self.id}'.",
                reason="Task already started",
                task=self,
            )

        self._task_desc = task_desc
        self._result.task_desc = task_desc
        if self._parent_task:
            self._task_desc.parent = self._parent_task.task_desc

    @property
    def parent_task(self) -> Optional["Task"]:
        return self._parent_task

    @parent_task.setter
    def parent_task(self, parent_task: "Task"):

        if self.is_started:
            raise FrklTaskException(
                msg=f"Can't set parent for task '{self.id}'.",
                reason="Task already started",
                task=self,
            )

        self._parent_task = parent_task
        self.task_desc.parent = self._parent_task.task_desc

    @property
    def is_started(self) -> bool:
        return self._started

    @property
    def is_finished(self) -> bool:

        return self._result.is_finished

    @abstractmethod
    def execute_task(self) -> Any:
        pass

    async def run_async(
        self, raise_exception: bool = False, msg: str = None, failed_msg: str = None
    ) -> TaskResult:

        if self._started:
            raise FrklTaskException(
                msg=f"Can't start task '{self.id}'.",
                reason="Task already started.",
                task=self,
            )

        if self.parent_task:

            if self.parent_task.task_desc.topic and not self.task_desc.topic:
                self._task_desc.topic = self.parent_task.task_desc.topic

        self._started = True
        self.task_desc.task_started(msg=msg, failed_msg=failed_msg)

        success: bool = True
        tz = get_localzone()

        try:
            self._start_time = tz.localize(datetime.now(), is_dst=None)

            if not self._executor:
                result_value = await self.execute_task()
            else:
                result_value = await self._executor.execute_task(self)

            error = None
        except Exception as e:
            log.debug(
                f"Error executing task '{self.task_desc.name}': {e}", exc_info=True
            )
            error = e
            result_value = None
            success = False

        self._end_time = tz.localize(datetime.now(), is_dst=None)

        if isinstance_or_subclass(result_value, TaskResult):
            result_value = result_value.result_value

        self._result.set_task_result(
            result_value=result_value,
            error=error,
            start_time=self._start_time,  # type: ignore
            end_time=self._end_time,  # type: ignore
            success=success,
        )

        self._success = success

        if not success:
            self.task_desc.task_failed(exception=error)
        else:
            self.task_desc.task_finished()

        if raise_exception and error:
            if isinstance_or_subclass(error, FrklTaskRunException):
                raise error

            raise FrklTaskRunException(self)

        return self.result

    @property
    def success(self):

        if not self.is_finished:
            raise FrklTaskException(
                msg=f"Can't check success status for task '{self.id}'.",
                reason="Task hasn't finished yet",
                task=self,
            )

        return self._success

    def __eq__(self, other):

        if not isinstance_or_subclass(other, Task):
            return False

        return self.id == other.id

    def __hash__(self):

        return hash(self.id)

    def __lt__(self, other):

        if not isinstance_or_subclass(other, Task):
            return False

        return (self.parent_task, self._created_time, self.id) < (
            other.parent_task,
            other._created_time,
            other.id,
        )

    def __repr__(self):

        return f"({self.__class__.__name__}: id={self.id})"


class SingleTaskAsync(Task):
    def __init__(
        self,
        func: Callable,
        func_args: Iterable[Any] = [],
        func_kwargs: Mapping[str, Any] = {},
        **kwargs: Any,
    ) -> None:

        super().__init__(**kwargs)

        self._func: Callable = func
        self._args: Iterable[Any] = func_args
        self._kwargs: Mapping[str, Any] = func_kwargs

    async def execute_task(self) -> Any:

        if inspect.iscoroutinefunction(self._func):
            result = await self._func(*self._args, **self._kwargs)
        else:
            result = self._func(*self._args, **self._kwargs)

        return result


class PostprocessTask(Task):
    def __init__(self, previous_task: Task, **kwargs):

        self._previous_task: Task = previous_task
        super().__init__(**kwargs)

    async def execute_task(self) -> Any:

        if not self._previous_task.is_finished:
            raise FrklTaskException(
                task=self,
                msg=f"Can't postproocess task '{self._previous_task.id}'.",
                reason="Task not finished yet.",
            )

        if not self._previous_task.success:
            exc = self._previous_task.result.error
            if exc is None:
                raise FrklException(
                    msg=f"Task {self._previous_task.task_desc.name} failed for unknown reasons."
                )
            else:
                raise exc

        result = await self.postprocess(self._previous_task)
        return result

    @abstractmethod
    async def postprocess(self, task: Task) -> Any:
        pass
