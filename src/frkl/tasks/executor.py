# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING, Any


if TYPE_CHECKING:
    from frkl.tasks.task import Task


class TaskExecutor(metaclass=ABCMeta):
    @abstractmethod
    async def execute_task(self, task: "Task") -> Any:
        pass


class SimpleExecutor(TaskExecutor):
    async def execute_task(self, task: "Task") -> Any:

        result = await task.execute_task()
        return result
