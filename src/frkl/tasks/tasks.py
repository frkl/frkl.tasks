# -*- coding: utf-8 -*-
from abc import abstractmethod
from typing import Any, Callable, Iterable, List, Optional, Union

from anyio import create_semaphore, create_task_group
from frkl.common.async_utils import wrap_async_task
from frkl.common.types import isinstance_or_subclass
from frkl.tasks.exceptions import FrklTaskException, FrklTasksRunException
from frkl.tasks.task import SingleTaskAsync, Task


def flatten_tasks(parent: Task) -> List[Task]:

    if not parent.has_subtasks:
        return [parent]

    result: List["Task"] = []
    for tasklet in parent.tasklets:  # type: ignore

        if not tasklet.has_subtasks:
            result.append(tasklet)
        else:
            c = flatten_tasks(tasklet)  # type: ignore
            result.extend(c)

    return result


class Tasks(Task):
    def __init__(self, *args, **kwargs):

        self._tasklets_prepared: bool = False
        self._tasklets: Optional[List[Task]] = None
        super().__init__(*args, **kwargs)

    def has_subtasks(self) -> bool:
        return True

    @property
    def tasklets(self) -> List[Task]:

        if self._tasklets is None:
            raise FrklTaskException(
                task=self,
                msg=f"Can't retrieve child tasks for '{self.id}'.",
                reason="Tasklets not initialized yet.",
            )
        return self._tasklets

    async def initialize_tasklets(self) -> None:
        """Method to overwrite to create child tasks in."""

        self._tasklets = []

    @abstractmethod
    async def execute_tasklets(self, *tasklets: Task) -> None:

        pass

    async def create_result_value(self, *tasklets: Task) -> Any:

        result_dict = {}
        for tasklet in tasklets:
            result_dict[tasklet.id] = tasklet.result
        return result_dict

    async def execute_task(self) -> Any:

        if self._tasklets is None:
            await self.initialize_tasklets()

        if self._tasklets is None:
            raise FrklTaskException(
                task=self,
                msg="Can't execute tasks.",
                reason="No child tasks created for this task. This is most likely a bug.",
            )

        # exceptions will be caught by Task
        await self.execute_tasklets(*self._tasklets)

        failed: bool = False
        for tasklet in self._tasklets:
            if not tasklet.result.success:
                failed = True
                break

        if failed:
            raise FrklTasksRunException(self)

        result = await self.create_result_value(*self._tasklets)
        return result

    def add_tasklet(self, tasklet: Union[Callable, Task]):

        if self._tasklets is None:
            raise FrklTaskException(
                task=self,
                msg=f"Can't add child task to '{self.id}'.",
                reason="Tasklets not initialized yet.",
            )

        if isinstance_or_subclass(tasklet, Task):
            _tasklet = tasklet
        elif callable(tasklet):
            _tasklet = SingleTaskAsync(func=tasklet)
        else:
            raise TypeError(
                f"Can't add tasklet: invalid type '{type(tasklet)}' (must be 'Task' or 'Callable'"
            )
        self._tasklets.append(_tasklet)  # type: ignore
        _tasklet.parent_task = self  # type: ignore
        if tasklet.has_subtasks and tasklet._tasklets is None:  # type: ignore
            wrap_async_task(tasklet.initialize_tasklets)  # type: ignore

    def add_tasklets(self, *tasklets: Union[Callable, Task]):
        for t in tasklets:
            self.add_tasklet(t)


class SimpleTasks(Tasks):
    def __init__(self, *args, tasklets: Optional[Iterable[Tasks]] = None, **kwargs):

        super().__init__(*args, **kwargs)
        self._tasklets = []
        if tasklets:
            self.add_tasklets(*tasklets)


class SerialTasks(SimpleTasks):
    async def execute_tasklets(self, *tasklets: Task) -> None:

        results = {}
        for t in tasklets:
            result = await t.run_async()
            results[t.id] = result.result_value


class ParallelTasksAsync(SimpleTasks):
    def __init__(
        self,
        *args,
        tasklets: Optional[Iterable[Tasks]] = None,
        max_parallel_tasks: Optional[int] = None,
        **kwargs,
    ):

        self._max_parallel_tasks: Optional[int] = max_parallel_tasks

        super().__init__(*args, tasklets=tasklets, **kwargs)

    async def execute_tasklets(self, *tasklets: Task) -> None:

        if self._max_parallel_tasks:

            async def wrap_task(_tasklet: Task, semaphore):
                async with semaphore:
                    await _tasklet.run_async()

            semaphore = create_semaphore(self._max_parallel_tasks)
            async with create_task_group() as tg:
                for tasklet in tasklets:
                    await tg.spawn(wrap_task, tasklet, semaphore)
        else:
            async with create_task_group() as tg:
                for tasklet in tasklets:
                    await tg.spawn(tasklet.run_async)


class FlattenParallelTasksAsync(SimpleTasks):
    async def execute_tasklets(self, *tasklets: Task) -> None:

        flattened = flatten_tasks(self)
        async with create_task_group() as tg:

            for child in flattened:
                await tg.spawn(child.run_async)


class FlattenSerialTasksAsync(SimpleTasks):
    async def execute_tasklets(self, *tasklets: Task) -> None:

        flattened = flatten_tasks(self)

        for child in flattened:
            await child.run_async()
