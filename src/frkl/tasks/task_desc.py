# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from datetime import datetime
from typing import Any, Dict, List, Mapping, Optional

from frkl.common.exceptions import FrklException
from frkl.common.strings import from_camel_case, generate_valid_identifier
from frkl.common.types import isinstance_or_subclass
from pubsub import pub
from tzlocal import get_localzone


class TaskDescPlugin(metaclass=ABCMeta):
    @abstractmethod
    def task_started(self, task_desc: "TaskDesc") -> None:
        pass

    @abstractmethod
    def task_finished(self, task_desc: "TaskDesc") -> None:
        pass

    @abstractmethod
    def task_failed(self, task_desc: "TaskDesc") -> None:
        pass


class PubSubTaskDescPlugin(TaskDescPlugin):
    def __init__(self, topic: str):

        self._base_topic = topic

    def task_started(self, task_desc: "TaskDesc") -> None:
        pub.sendMessage(
            self._base_topic,
            event_name="task_started",
            source=task_desc,
            event_details={},
        )

    def task_finished(self, task_desc: "TaskDesc") -> None:

        pub.sendMessage(
            self._base_topic,
            event_name="task_finished",
            source=task_desc,
            event_details={},
        )

    def task_failed(self, task_desc: "TaskDesc") -> None:

        pub.sendMessage(
            self._base_topic,
            event_name="task_failed",
            source=task_desc,
            event_details={},
        )


class TaskDesc(object):
    def __init__(
        self,
        id: Optional[str] = None,
        name: Optional[str] = None,
        msg: Optional[str] = None,
        failed_msg: Optional[str] = None,
        topic: Optional[str] = None,
        created: Optional[datetime] = None,
        parent: Optional["TaskDesc"] = None,
        has_subtasks: Optional[bool] = None,
        priority: Optional[int] = None,
        **kwargs: Any,
    ) -> None:

        self._name: Optional[str] = name
        if self._name is None:
            prefix = from_camel_case(self.__class__.__name__)
        else:
            prefix = self._name
        if id is None:
            id = generate_valid_identifier(prefix=f"{prefix}_", length_without_prefix=8)
        self._id: str = id
        tz = get_localzone()
        if created is None:
            created = tz.localize(datetime.now(), is_dst=None)
        self._created_time: datetime = created

        self._parent: Optional[TaskDesc] = parent

        self._has_subtasks: Optional[bool] = has_subtasks
        self._priority: Optional[int] = priority

        self._msg: Optional[str] = msg
        self._failed_msg: Optional[str] = failed_msg
        self._result_msg: Optional[str] = None
        self._exception: Optional[Exception] = None

        self._sub_tasks: Mapping[datetime, TaskDesc] = {}

        self._start_time: Optional[datetime] = None
        self._end_time: Optional[datetime] = None

        self._failed: bool = False

        self._plugins: Dict[str, TaskDescPlugin] = {}

        self._topic: Optional[str] = None
        self.topic = topic  # make sure to set pubsubplugin

    @property
    def id(self) -> str:
        return self._id

    @property
    def name(self) -> str:
        if not self._name:
            return self._id
        return self._name

    @name.setter
    def name(self, name: str) -> None:
        self._name = name

    @property
    def topic(self) -> Optional[str]:

        return self._topic

    @topic.setter
    def topic(self, topic: Optional[str] = None):

        self._topic = topic
        if topic and topic not in self._plugins.keys():
            self._plugins[topic] = PubSubTaskDescPlugin(topic=self.topic)  # type: ignore

    @property
    def priority(self) -> int:

        if self._priority is not None:
            return self._priority

        current_parent: Optional[TaskDesc] = self.parent
        current_level = 1
        while current_parent is not None:
            current_level += 1
            current_parent = current_parent.parent

        return current_level * 10

    @priority.setter
    def priority(self, priority: Optional[int]) -> None:
        self._priority = priority

    @property
    def parent(self) -> Optional["TaskDesc"]:

        return self._parent

    @parent.setter
    def parent(self, parent: "TaskDesc") -> None:

        if self._start_time:
            raise Exception("Can't set parent after task started.")
        self._parent = parent

    # def add_plugins(self, *plugins: TaskDescPlugin) -> None:
    #
    #     for p in plugins:
    #         self._plugins.append(p)

    @property
    def msg(self) -> Optional[str]:

        if self._msg is None:
            return f"executing: {self.name}"

        return self._msg

    @msg.setter
    def msg(self, msg: str) -> None:

        if self._msg:
            raise Exception(
                f"Message already set for task desc '{self.name}': '{self.msg}'"
            )

        self._msg = msg

    def get_failed_msg(self, default: str = None) -> str:

        if self._failed_msg is None:
            if self._exception is not None:
                if isinstance_or_subclass(self._exception, FrklException):
                    return self._exception.message_short  # type: ignore
                else:
                    return str(self._exception)
            else:
                if default is None:
                    return f"failed: {self.name}"
                else:
                    return default

        return self._failed_msg

    def set_failed_msg(self, msg: str) -> None:

        if self._failed_msg:
            raise Exception(
                f"Failed message already set for task desc '{self.name}': '{self.msg}'"
            )

        self._failed_msg = msg

    def get_result_msg(self, default: str = None) -> str:

        if self._result_msg is None:
            if default is None:
                return f"finished: {self.name}"
            else:
                return default

        return self._result_msg

    @property
    def namespace(self) -> str:

        if not self._start_time:
            raise Exception("Can't get namespace before task has started.")

        current_parent: Optional[TaskDesc] = self.parent

        tokens: List[str] = []
        while True:
            if current_parent:
                tokens.insert(0, current_parent.id)
            else:
                break
            current_parent = self.parent.parent  # type: ignore

        return ".".join(tokens)

    def task_started(self, msg: str = None, failed_msg: str = None) -> None:

        self._start_time = datetime.now()
        if msg is not None:
            self.msg = msg
        if failed_msg is not None:
            self.set_failed_msg(failed_msg)

        for plugin in self._plugins.values():
            plugin.task_started(self)

    def task_finished(self, msg: str = None) -> None:

        self._end_time = datetime.now()
        self._result_msg = msg

        for plugin in self._plugins.values():
            plugin.task_finished(self)

    def is_started(self) -> bool:

        return self._start_time is not None

    def is_finished(self) -> bool:

        return self._end_time is not None

    def task_failed(self, failed_msg: str = None, exception: Exception = None) -> None:

        self._end_time = datetime.now()
        if failed_msg is not None:
            self._failed_msg = failed_msg
        self._exception = exception

        for plugin in self._plugins.values():
            plugin.task_failed(self)

        # if self.parent and failed_msg is not None:
        #     if self.parent._failed_msg is None:
        #         self.parent.set_failed_msg(failed_msg)

    def __repr__(self):

        return (
            f"[{self.__class__.__name__}: name={self.name} msg={self.msg} id={self.id}"
        )
