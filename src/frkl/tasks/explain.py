# -*- coding: utf-8 -*-
from typing import Any, Dict, List, Mapping

from frkl.common.defaults import FRKL_COLOR_PROGRESSION
from frkl.common.formats.serialize import make_serializable, to_value_string
from frkl.explain.explanation import DataExplanation
from frkl.explain.explanations.exception import ExceptionExplanation
from frkl.tasks.task import Task, TaskResult
from frkl.tasks.task_desc import TaskDesc
from rich.console import Console, ConsoleOptions, RenderResult


INDENT = 2


def get_tree_nodes(node: Mapping[str, Any], level: int = 0, current: List[str] = None):

    if current is None:
        current = []

    padding = " " * INDENT * level

    if level == 0:
        color = ""
        color_end = ""
    else:
        color_number = (level - 1) % 5
        color_name = FRKL_COLOR_PROGRESSION[color_number]
        color = f"[{color_name}]"
        color_end = f"[/{color_name}]"

    msg = node["meta"].get("msg", node["meta"].get("id", "-- n/a --"))
    current.append(f"{padding}- {color}{msg}{color_end}")

    for subtask in node.get("subtasks", []):
        get_tree_nodes(subtask, level=level + 1, current=current)

    return current


class TaskExplanation(DataExplanation):
    async def create_explanation_data(self) -> Mapping[str, Any]:

        task: Task = self.data
        task_desc: TaskDesc = task.task_desc

        result: Dict[str, Any] = {
            "meta": {"id": task.id, "name": task_desc.name, "msg": task_desc.msg}
        }
        if task.has_subtasks:
            subtasks = []
            for child in task.tasklets:  # type: ignore
                st_expl = TaskExplanation(child)
                subtasks.append(st_expl)

            result["subtasks"] = subtasks

        return result

    def __rich_console__(
        self, console: Console, options: ConsoleOptions
    ) -> RenderResult:

        yield "[bold bright_black]Steps[/bold bright_black]"
        yield ""
        indent = self.config_value("indent", 0)
        start_level = int(indent / INDENT)
        tree_nodes = get_tree_nodes(self.serializable_data, level=start_level)
        for t in tree_nodes:
            yield t
        return


class TaskResultExplanation(DataExplanation):
    async def create_explanation_data(self) -> Mapping[str, Any]:

        task_result: TaskResult = self.data

        result = {}
        if task_result.success:
            result["value"] = task_result.result_value
        else:
            result["error"] = ExceptionExplanation(task_result.error)

        return result

    def __rich_console__(
        self, console: "Console", options: "ConsoleOptions"
    ) -> "RenderResult":

        task_result: TaskResult = self.data
        if task_result.success:
            tmp = make_serializable(task_result.result_value)
            value_string = to_value_string(tmp)
            print(value_string)
            return ""
        else:
            expl_exc = ExceptionExplanation(task_result.error)
            return expl_exc.create_exception_text()
