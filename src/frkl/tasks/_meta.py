# -*- coding: utf-8 -*-
from typing import Any, Dict


project_name = "frkl.tasks"
project_main_module = "frkl.tasks"
project_slug = "frkl_tasks"

pyinstaller: Dict[str, Any] = {
    "hiddenimports": [
        "frkl.tasks.task_watchers",
        "frkl.tasks.task_watchers.terminal",
        "frkl.tasks.task_watchers.simple",
        "frkl.tasks.task_watchers.tree",
    ]
}
