# -*- coding: utf-8 -*-
from typing import TYPE_CHECKING, Iterable, List, Optional

from frkl.common.exceptions import FrklExtendedException, FrklMultiParentException


if TYPE_CHECKING:
    from frkl.tasks.tasks import Task, Tasks


def get_failed_msgs(task: "Task", msgs: Optional[List[str]] = None) -> List[str]:

    if msgs is None:
        msgs = []

    if not task.has_subtasks:

        if not task.success:
            _m = task.task_desc.get_failed_msg()
            if _m not in msgs:
                msgs.append(_m)
        return msgs

    else:

        for child in task.tasklets:  # type: ignore
            _n = get_failed_msgs(child)
            msgs.extend(_n)

        return msgs


class FrklTaskException(FrklExtendedException):
    def __init__(self, task: "Task", **kwargs):

        self._task: "Task" = task
        if "parent" not in kwargs.keys():
            if self._task.is_finished:
                kwargs["parent"] = self._task.result.error

        super().__init__(**kwargs)

    @property
    def task(self) -> "Task":
        return self._task


class FrklTaskRunException(FrklTaskException):
    def __init__(self, task: "Task", **kwargs):

        super().__init__(task=task, **kwargs)


class FrklTasksRunException(FrklTaskRunException):
    def __init__(self, task: "Tasks", **kwargs):

        self._failed_tasks: List[Task] = []
        for t in task.tasklets:
            if not t.is_started:
                continue
            if not t.result.success:
                self._failed_tasks.append(t)

        if len(self._failed_tasks) == 1:
            # exc: Exception = self._failed_tasks[0].result.error  # type: ignore
            super().__init__(task=task, parent=self._failed_tasks[0].result.error)

        else:
            if "msg" not in kwargs.keys():
                kwargs["msg"] = "Multiple child tasks failed."

            reason: List[Exception] = []
            for f in self._failed_tasks:
                if f.result.error is not None:
                    reason.append(f.result.error)
                else:
                    raise Exception(
                        f"Failed task '{f.task_desc.name}' does not have exception attached. This is a bug."
                    )

            parent = FrklMultiParentException(*reason, **kwargs)

            super().__init__(task=task, parent=parent, **kwargs)

    def failed_tasks(self) -> Iterable["Task"]:
        return self._failed_tasks
